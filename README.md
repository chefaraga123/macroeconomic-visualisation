# Macroeconomic  Visualisation


Going through the second year Oxford Macroeconomic course, providing 
visualisation tools for concepts learnt throughout the course. 

The final project will go linearly through the course providing visualisation tools
to help students intuit the relationship between macroeconomic variables. 

Parts of the course:

- Monetary Policy in the closed economy 
- Monetary Policy in the open economy 
- Growth Models
- Fiscal Policy 
- Consumption Models


Best Content:

- macro.mp4 - animation dynamically showing IS-PC-MR relationship 
- IS_PC_MR_Curves_new.png - diagram showing IS-PC-MR relationship
- price-paths with shocks and corrective monetary policy 
- Solow_Model.png - shows Solow model with steady state 


Future Updates:
- I shall carry on with the project when I am back at university 
(and have actually started learning second-year macro formally!)
