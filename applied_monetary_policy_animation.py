import random
import math
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.animation import writers
import pandas as pd 
import numpy as np 

#global variables 

class IS_Curve:

    def __init__(self,stabilising_interest_rate,alpha,equilibrium_output):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.alpha = alpha
        self.equilibrium_output = equilibrium_output

    def primary_IS_Relationship(self,previous_period_real_interest_rate):
        monetary_policy_decision = previous_period_real_interest_rate - self.stabilising_interest_rate
        output = self.equilibrium_output - self.alpha*(monetary_policy_decision) 
        return output 
    
    def Secondary_IS_Relationship(self,new_output):
        output_gap = self.equilibrium_output - new_output
        interest_rate =  output_gap/self.alpha + self.stabilising_interest_rate
        return interest_rate
    
    def IS_Schedule(self):
        output_schedule = []
        interest_rates = []
        for previous_period_real_interest_rate in range(0,11):
            actual_output = self.primary_IS_Relationship(previous_period_real_interest_rate)
            interest_rates.append(previous_period_real_interest_rate)
            output_schedule.append(actual_output)
        return [output_schedule,interest_rates]



class P_Curve:    

    def __init__(self,equilibrium_output,alpha,last_period_inflation):
        self.equilibrium_output = equilibrium_output
        self.alpha = alpha 
        self.last_period_inflation = last_period_inflation

    def P_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output
        inflation = self.alpha*output_gap + self.last_period_inflation
        return inflation

    def PC_Schedule(self):
        inf_schedule = [] 
        actual_outputs = []
        for actual_output in range(95,110):
            inf = self.P_Relationship(actual_output)#+shift
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]

    
class MR_Curve:

    def __init__(self,target_inflation,equilibrium_output,beta,alpha):
        self.target_inflation = target_inflation
        self.equilibrium_output = equilibrium_output
        self.beta = beta
        self.alpha = alpha

    def primary_MR_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output 
        inflation = self.target_inflation - output_gap/(self.beta*self.alpha)
        return inflation 

    def MR_Relationship(self,current_inflation):
        inflation_deviation = self.target_inflation - current_inflation
        gdp_new =  (self.beta*self.alpha)*(inflation_deviation) + self.equilibrium_output
        return gdp_new

    def MR_Schedule(self):
        inf_schedule = [] 
        actual_outputs = [] 
        for actual_output in range(95,110): 
            inf = self.primary_MR_Relationship(actual_output)
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]



class schedule_construction:

    def __init__(self,stabilising_interest_rate,
                alpha,equilibrium_output,
                target_inflation,beta):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.equilibrium_output = equilibrium_output
        self.target_inflation = target_inflation
        self.beta = beta
        self.alpha = alpha

        self.inflation_expectations = []
        self.interest_rates = []
        self.inflation_rates = [] 
        self.outputs = []



    def Economy_State_Change(self,interest_rate,last_period_inflation,
                        time_period,time_limit):
        
        if time_period <= time_limit:
            #shock implementation
            if time_period == 2:
                shocked_output = self.equilibrium_output + 4
                IS_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,shocked_output)
            else:
                IS_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,self.equilibrium_output)

            current_output = IS_curve.primary_IS_Relationship(interest_rate)
            #adaptive expectations phillips curve 
            P_curve = P_Curve(self.equilibrium_output,self.alpha,last_period_inflation)
            new_inflation = P_curve.P_Relationship(current_output) #becomes inflation expectations
            #becomes last period inflation
            
            MR_curve = MR_Curve(self.target_inflation,self.equilibrium_output,self.beta,self.alpha)
            new_output = MR_curve.MR_Relationship(new_inflation)
            
            new_interest_rate = IS_curve.Secondary_IS_Relationship(new_output)
            
            self.inflation_expectations.append(last_period_inflation)
            self.interest_rates.append(new_interest_rate)
            self.inflation_rates.append(new_inflation),
            self.outputs.append(current_output)

            #for each time period interest rate, inflation and the time period are updated
            #gdp exists as a function of the other parameters -nothing on its own
            self.Curve_Schedules(new_interest_rate,new_inflation,time_period+1,time_limit)
            self.Economy_State_Change(new_interest_rate,new_inflation,time_period+1,time_limit)
        else:
            indices.append(self.inflation_expectations)
            indices.append(self.interest_rates)
            indices.append(self.inflation_rates)
            indices.append(self.outputs)


    def Curve_Schedules(self,interest_rate,last_period_inflation,
                        time_period,time_limit):#just calls all the schedules for the 
        
        is_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,self.equilibrium_output)
        schedules.append(is_curve.IS_Schedule())

        p_curve = P_Curve(self.equilibrium_output,self.alpha,last_period_inflation)
        schedules.append(p_curve.PC_Schedule())
        
        mr_curve = MR_Curve(self.target_inflation,self.equilibrium_output,self.beta,self.alpha)
        schedules.append(mr_curve.MR_Schedule())

#parameters    
stabilising_interest_rate = 5
alpha = 1
beta = 1
target_inflation = 2
last_period_inflation = target_inflation
time_period = 1
equilibrium_output = 100
indices = []
schedules = []


def data_creation():
    Schedule_Construction = schedule_construction(stabilising_interest_rate,
                        alpha,equilibrium_output,target_inflation,beta)
    #instantiates schedule construction
    Schedule_Construction.Economy_State_Change(5,2,1,10) #generates time series data 
    #indices can be used as input for an animation 
    Schedule_Construction.Curve_Schedules(5,2,1,10) 

    #generates data for a single turn


data_creation() #updates indicies and schedules 
#indices, schedules both lists of lists ready to be plotted on a subplot 
'''



ax1.plot(schedules[0][0],schedules[0][1])
ax1.set_ylabel('Interest Rate')

ax2.plot(schedules[1][0],schedules[1][1])
ax2.plot(schedules[2][0],schedules[2][1])
ax2.set_ylabel('Inflation')


'''

#add all subplots 
fig = plt.figure(figsize=(20,10))
#plt.title("Monetary Policy")
ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 3)

ax3 = fig.add_subplot(4, 2, 2)
ax4 = fig.add_subplot(4, 2, 4)
ax5 = fig.add_subplot(4, 2, 6)
ax6 = fig.add_subplot(4, 2, 8)


Line1 = ax1.plot([],label="IS-Curve")
line1 = Line1[0]
ax1.set_title("Interest-Rates vs Inflation")
ax1.set_xlim(92,107)
ax1.set_ylim(-1,11)


Line2 = ax2.plot([],label="Phillips Curve")
line2 = Line2[0]
Line2a = ax2.plot([],label="Monetary Rule")
line2a = Line2a[0]
ax2.set_title("Inflation vs Output")
ax2.set_xlim(93,112)
ax2.set_ylim(-5,10)


Line3 = ax3.plot([],label="Inflation")
line3 = Line3[0]
ax3.set_title("Inflation vs Time")
ax3.set_xlim(0,10)
ax3.set_ylim(-5,10)

Line4 = ax4.plot([], label="GDP Index")
line4 = Line4[0]
ax4.set_title("GDP Index vs Time")
ax4.set_xlim(0,10)
ax4.set_ylim(90,110)

Line5 = ax5.plot([], label="Inflation Expectations")
line5 = Line5[0]
ax5.set_title("Inflation Expectations vs Time")
ax5.set_xlim(0,10)
ax5.set_ylim(-5,10)

Line6 = ax6.plot([], label="Interest Rate")
line6 = Line6[0]
ax4.set_title("Interest Rate vs Time")
ax6.set_xlim(0,10)
ax6.set_ylim(0,15)


x_data3 = []
x_data4 = []
x_data5 = []
x_data6 = []

interest_rate_schedules = []
PC_schedules = []
MR_schedules = [] 

for pos,schedule in enumerate(schedules):
    
    if str((pos+3)/3)[-1]=='0':
        print('test')
        interest_rate_schedules.append(schedule)
        print('interest_rate_schedules',schedule)
    elif str((pos+2)/3)[-1]=='0':
        PC_schedules.append(schedule)
        print('PC_schedules',schedule)
    elif str((pos+1)/3)[-1]=='0':
        MR_schedules.append(schedule)
        print('MR_Schedule',schedule)


def animation_frame(i):
    
    #Animate Interest Rate Relationships
    y1 = interest_rate_schedules[i][1]
    x1 = interest_rate_schedules[i][0]
    line1.set_data(x1,y1)  
    
    #Animate Inflation Relationships
    y2 = PC_schedules[i][1]
    x2 = PC_schedules[i][0]
    line2.set_data(x2,y2)

    y2a = MR_schedules[i][1]
    x2a = MR_schedules[i][0]
    line2a.set_data(x2a,y2a)  
    
    #Animate Inflation Time-Series 
    y3 = indices[2][i]
    x3 = np.linspace(i, i+1, 2)
    x_data3.append(x3)
    line3.set_data(x_data3,y3)  

    #Animate GDP-Index Time-Series
    y4 = indices[3][i]
    x4 = np.linspace(i, i+1, 2)
    x_data4.append(x4)
    line4.set_data(x_data4,y4)

    #Animate Inflation-Expectations Time-Series
    y5 = indices[0][i]
    x5 = np.linspace(i, #schedules length is 33 
i+1, 2)
    x_data5.append(x5)
    line5.set_data(x_data5,y5)

    #Animate Interest Rate Time-Series 
    y6 = indices[1][i]
    x6 = np.linspace(i, i+1, 2)
    x_data6.append(x6)
    line6.set_data(x_data6,y6)
    
    ax6.legend()
    ax5.legend()
    ax4.legend()
    ax3.legend()
    ax2.legend()
    ax1.legend()


Writer = writers['ffmpeg']
writer = Writer(fps=5, metadata=dict(artist='Me'), bitrate=1800)

animation = FuncAnimation(fig, func=animation_frame, frames=np.arange(0,10,1), interval=300)
animation.save('Diagrams/macro.mp4', writer=writer)
plt.show()
plt.close()

