import random
import math
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import pandas as pd 
import numpy as np 

#Actions:
'''
    - perform action on IS-Curve (Demand shock)
    - shift in IS-Curve leads to shift of Phillips Curve 
    - Shift of Phillips Curve leads to change in PC-MR intersection 
    - Leads to a change in interest rates 
    - The change in interest rates leasd to a change in inflation expectations, 
    - Leading to reverting shift of the Phillips curve
    - This defines a new PC-MR intersection 
    - This leads to new optimal interest rates 
    - This leads to a change in inflation etc.. 

Is Curve shifts once, leading to a shift of the phillips curve which leads to a change 
in IR, which leads to a shift of the Phillips curve which leads to a change in IR etc.. 
until equilibrium when IR = equilibrium interest rate & inflation = target inflation 
'''

#3 classes representing each relationship/curve 


class IS_Curve:

    def __init__(self,stabilising_interest_rate,alpha,equilibrium_output):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.alpha = alpha
        self.equilibrium_output = equilibrium_output

    def external_shock(self):
        return 4 #np.random.normal(5, 2) # returns a shock value according to a normal distribution 

    def IS_Shock(self,output):
        return output + self.external_shock

    def IS_Relationship(self,previous_period_real_interest_rate):
        monetary_policy_decision = previous_period_real_interest_rate - self.stabilising_interest_rate
        output = self.equilibrium_output - self.alpha*(monetary_policy_decision) 
        return output 

    def IS_Relationship_2(self,actual_output):
        output_gap = self.equilibrium_output - actual_output
        return output_gap/self.alpha + self.stabilising_interest_rate

    def IS_Schedule(self):
        output_schedule = []
        interest_rates = []
        for previous_period_real_interest_rate in range(0,11):
            actual_output = self.IS_Relationship(previous_period_real_interest_rate)
            interest_rates.append(previous_period_real_interest_rate)
            output_schedule.append(actual_output)
        return [output_schedule,interest_rates]
            #output interest rate given output

    def plot_IS_Curve_equilibrium(self):
        IS_curve = self.IS_Schedule()
        plt.plot(IS_curve[0], IS_curve[1], label="Initial")

    def plot_IS_curve_shock(self):
        IS_curve = self.IS_Schedule()
        shock_schedule = [] 
        shock = self.external_shock()
        for val in a[0]:
            shock_schedule.append(IS_Shock(val,shock))
        plt.plot(shock_schedule, IS_curve[1], label="Shock")


class P_Curve:    

    def __init__(self,equilibrium_output,alpha,last_period_inflation):
        self.equilibrium_output = equilibrium_output
        self.alpha = alpha 
        self.last_period_inflation = last_period_inflation

    def PC_Relationship(self,actual_output): #calculates inflation 
        output_gap = actual_output - self.equilibrium_output
        inflation = self.alpha*output_gap + self.last_period_inflation
        return inflation

    def PC_Schedule(self,shift):
        inf_schedule = [] 
        actual_outputs = []
        for actual_output in range(95,110):
            inf = self.PC_Relationship(actual_output)+shift
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]

    def plot_P_Curve(self,x):
        inflation_schedule = self.PC_Schedule(x)
        plt.plot(inflation_schedule[0],inflation_schedule[1], label="Phillips Curve")


class MR_Curve:

    def __init__(self,target_inflation,equilibrium_output,beta,alpha):
        self.target_inflation = target_inflation
        self.equilibrium_output = equilibrium_output
        self.beta = beta
        self.alpha = alpha

    def primary_MR_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output 
        inflation = self.target_inflation - output_gap/(self.beta*self.alpha)
        return inflation 

    def MR_Relationship(self,inflation):
        #   1 X (inflation deviation) X equilibrium output - this turn output 
        return (self.beta*self.alpha)*(self.target_inflation - inflation) + self.equilibrium_output

    def MR_Schedule(self):
        inf_schedule = [] 
        actual_outputs = [] 
        for actual_output in range(95,110): 
            inf = self.primary_MR_Relationship(actual_output)
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]
    
    def plot_MR_Curve(self):
        inflation_schedule = self.MR_Schedule()
        plt.plot(inflation_schedule[0],inflation_schedule[1], label="MR Curve")


equilibrium_output = 100#index
equilibrium_interest_rate = 5#%
alpha = 1
beta = 1
previous_period_real_interest_rate = equilibrium_interest_rate
target_inflation = 2#%
last_period_inflation = target_inflation
time_period = 1


#generate a data set ahead of time then animate it

#reinstantiate objects for each time period other than the MR Curve 


#MR_Curve = MR_Curve(target_inflation,equilibrium_output,beta,alpha)
#P_Curve_1 = P_Curve(equilibrium_output,alpha,target_inflation)
#IS_Curve_1 = IS_Curve(equilibrium_interest_rate,alpha,equilibrium_output)

plots = [] 

def generate_time_series_interaction(MR_Curve,P_Curve,IS_Curve,
    equilibrium_interest_rate,alpha,target_inflation,
    previous_period_real_interest_rate,beta, time_period,
    equilibrium_output,last_period_inflation):
    if time_period <= 10:

        if time_period == 2: #shock one turn with +5 output
            x = 5
            IS_Curve_1 = IS_Curve(equilibrium_interest_rate,alpha,equilibrium_output+x)
            economy_state = IS_Curve_1.IS_Relationship(previous_period_real_interest_rate)
            #economy at 105 ceteris paribus

            P_Curve_1 = P_Curve(equilibrium_output,alpha,last_period_inflation)
            inflation_state = P_Curve_1.PC_Relationship(economy_state) #current period inflation not affected?
            #changes the output of the following state
            PC_schedule_timeseries = P_Curve_1.PC_Schedule(inflation_state)

            MR_Curve_1 = MR_Curve(target_inflation,equilibrium_output,beta,alpha)
            output_1 = MR_Curve_1.MR_Relationship(inflation_state)#give output

            Interest_rate = IS_Curve_1.IS_Relationship_2(output_1)
            print(Interest_rate)

            IS_schedule_timeseries = IS_Curve_1.IS_Schedule()
            MR_schedule_timeseries = MR_Curve_1.MR_Schedule()

            plots.append([IS_schedule_timeseries, PC_schedule_timeseries, MR_schedule_timeseries])

        else:
            x=0
            IS_Curve_1 = IS_Curve(equilibrium_interest_rate,alpha,equilibrium_output)
            economy_state = IS_Curve_1.IS_Relationship(previous_period_real_interest_rate)
            IS_schedule_timeseries = IS_Curve_1.IS_Schedule()

            P_Curve_1 = P_Curve(equilibrium_output,alpha,last_period_inflation)
            inflation_state = P_Curve_1.PC_Relationship(economy_state) 
            #changes the output of the following state
            PC_schedule_timeseries = P_Curve_1.PC_Schedule(inflation_state)

            MR_Curve_1 = MR_Curve(target_inflation,equilibrium_output,beta,alpha)
            output_1 = MR_Curve_1.MR_Relationship(inflation_state)
            Interest_rate = IS_Curve_1.IS_Relationship_2(output_1)
            print(Interest_rate)
            MR_schedule_timeseries = MR_Curve_1.MR_Schedule()

        #end
            plots.append([IS_schedule_timeseries, PC_schedule_timeseries, MR_schedule_timeseries])
        #print('IS_schedule_timeseries',IS_schedule_timeseries,'PC_schedule_timeseries',PC_schedule_timeseries, 'MR_schedule_timeseries',MR_schedule_timeseries)


        
        generate_time_series_interaction(MR_Curve,P_Curve,IS_Curve,
    equilibrium_interest_rate,alpha,target_inflation,
    Interest_rate,beta, time_period+1,equilibrium_output,last_period_inflation)

generate_time_series_interaction(MR_Curve,P_Curve,IS_Curve,
    equilibrium_interest_rate,alpha,target_inflation,
    previous_period_real_interest_rate,beta, time_period,
    equilibrium_output,target_inflation)

#print(plots)

def animate(time_period,schedule_timeseries):
    a = schedule_timeseries[0][0]
    b = schedule_timeseries[0][1]
    c = schedule_timeseries[0][2]
    z = schedule_timeseries[time_period]



    plt.figure(figsize=(16,6))
    plt.plot(a[0],a[1],label="Equilibrium Investment-Savings Curve")
    plt.plot(z[0][0],z[0][1], label="Investment-Savings Curve")
    plt.scatter(100,5)
    plt.annotate("Equilibrium", (100, 5))
    plt.title("IS Curve")
    plt.legend()
    plt.show()


    plt.figure(figsize=(16,6))
    plt.plot(b[0],b[1],label="Equilibrium Phillips Curve")
    plt.plot(z[1][0], z[1][1], label="Phillips Curve")
    plt.plot(z[2][0], z[2][1], label="Monetary Rule Curve")
    plt.scatter(100,2)
    plt.annotate("bliss point", (100, 2))#seems like target inflation what accounted for twice
    plt.title("MR-P Curve")
    plt.legend()
    plt.show()



'''
# Create two subplots and unpack the output array immediately
f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
ax1.plot(x, y)
ax1.set_title('Sharing Y axis')
ax2.scatter(x, y)


'''
'''
f, (ax_1,ax_2) = plt.subplots(1,2) #perhaps share an x axis 



ax_1.set_xlim(90,110)
ax_1.set_ylim(0,10)#interest rates
ax_1.set_title("Investment-Savings")

ax_2.set_xlim(90,110)
ax_2.set_ylim(0,10)#inflation rate
ax_2.set_title("Monetary Response")
'''

for time_period in range(0,5):
    animate(time_period,plots)

plt.show()


#'FuncAnimation(fig_1, animate, frames=10, interval=500)'
#'FuncAnimation(fig_2, animate, frames=10, interval=500)'


#need to implement animated subplots 
