import matplotlib.pyplot as plt

#global variables 

class IS_Curve:

    def __init__(self,stabilising_interest_rate,alpha,equilibrium_output):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.alpha = alpha
        self.equilibrium_output = equilibrium_output

    def primary_IS_Relationship(self,previous_period_real_interest_rate):
        monetary_policy_decision = previous_period_real_interest_rate - self.stabilising_interest_rate
        output = self.equilibrium_output - self.alpha*(monetary_policy_decision) 
        return output 
    
    def Secondary_IS_Relationship(self,new_output):
        output_gap = self.equilibrium_output - new_output
        interest_rate =  output_gap/self.alpha + self.stabilising_interest_rate
        return interest_rate
    
    def IS_Schedule(self):
        output_schedule = []
        interest_rates = []
        for previous_period_real_interest_rate in range(0,11):
            actual_output = self.primary_IS_Relationship(previous_period_real_interest_rate)
            interest_rates.append(previous_period_real_interest_rate)
            output_schedule.append(actual_output)
        return [output_schedule,interest_rates]



class P_Curve:    

    def __init__(self,equilibrium_output,alpha,last_period_inflation):
        self.equilibrium_output = equilibrium_output
        self.alpha = alpha 
        self.last_period_inflation = last_period_inflation

    def P_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output
        inflation = self.alpha*output_gap + self.last_period_inflation
        return inflation

    def PC_Schedule(self):
        inf_schedule = [] 
        actual_outputs = []
        for actual_output in range(95,110):
            inf = self.P_Relationship(actual_output)#+shift
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]

    
class MR_Curve:

    def __init__(self,target_inflation,equilibrium_output,beta,alpha):
        self.target_inflation = target_inflation
        self.equilibrium_output = equilibrium_output
        self.beta = beta
        self.alpha = alpha

    def primary_MR_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output 
        inflation = self.target_inflation - output_gap/(self.beta*self.alpha)
        return inflation 

    def MR_Relationship(self,current_inflation):
        inflation_deviation = self.target_inflation - current_inflation
        gdp_new =  (self.beta*self.alpha)*(inflation_deviation) + self.equilibrium_output
        return gdp_new

    def MR_Schedule(self):
        inf_schedule = [] 
        actual_outputs = [] 
        for actual_output in range(95,110): 
            inf = self.primary_MR_Relationship(actual_output)
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]



class schedule_construction:

    def __init__(self,stabilising_interest_rate,
                alpha,equilibrium_output,
                target_inflation,beta):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.equilibrium_output = equilibrium_output
        self.target_inflation = target_inflation
        self.beta = beta
        self.alpha = alpha

        self.inflation_expectations = []
        self.interest_rates = []
        self.inflation_rates = [] 
        self.outputs = []



    def Economy_State_Change(self,interest_rate,last_period_inflation,
                        time_period,time_limit):
        
        if time_period <= time_limit:
            #shock implementation
            if time_period == 2:
                shocked_output = self.equilibrium_output + 4
                IS_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,shocked_output)
            else:
                IS_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,self.equilibrium_output)

            current_output = IS_curve.primary_IS_Relationship(interest_rate)
            #adaptive expectations phillips curve 
            P_curve = P_Curve(self.equilibrium_output,self.alpha,last_period_inflation)
            new_inflation = P_curve.P_Relationship(current_output) #becomes inflation expectations
            #becomes last period inflation
            
            MR_curve = MR_Curve(self.target_inflation,self.equilibrium_output,self.beta,self.alpha)
            new_output = MR_curve.MR_Relationship(new_inflation)
            
            new_interest_rate = IS_curve.Secondary_IS_Relationship(new_output)
            
            self.inflation_expectations.append(last_period_inflation)
            self.interest_rates.append(new_interest_rate)
            self.inflation_rates.append(new_inflation),
            self.outputs.append(current_output)

            #for each time period interest rate, inflation and the time period are updated
            #gdp exists as a function of the other parameters -nothing on its own
            self.Economy_State_Change(new_interest_rate,new_inflation,time_period+1,time_limit)
        else:
            indices.append(self.inflation_expectations)
            indices.append(self.interest_rates)
            indices.append(self.inflation_rates)
            indices.append(self.outputs)


    def Curve_Schedules(self,interest_rate,last_period_inflation,
                        time_period,time_limit):#just calls all the schedules for the 
        
        is_curve = IS_Curve(self.stabilising_interest_rate,self.alpha,self.equilibrium_output)
        schedules.append(is_curve.IS_Schedule())

        p_curve = P_Curve(self.equilibrium_output,self.alpha,last_period_inflation)
        schedules.append(p_curve.PC_Schedule())
        
        mr_curve = MR_Curve(self.target_inflation,self.equilibrium_output,self.beta,self.alpha)
        schedules.append(mr_curve.MR_Schedule())

#parameters    
stabilising_interest_rate = 5
alpha = 1
beta = 1
target_inflation = 2
last_period_inflation = target_inflation
time_period = 1
equilibrium_output = 100
indices = []
schedules = []

def data_creation():
    Schedule_Construction = schedule_construction(stabilising_interest_rate,
                        alpha,equilibrium_output,target_inflation,beta)
    #instantiates schedule construction
    Schedule_Construction.Economy_State_Change(5,2,1,10) #generates time series data 
    #indices can be used as input for an animation 
    Schedule_Construction.Curve_Schedules(5,2,1,10) 
    #generates data for a single turn
    #need a way of linking the economy state generator to curve schedules 

    #schedules can be used as input for an animation 
    #generates time-series of macroeconomic variables 

data_creation() #updates indicies and schedules 
#indices, schedules both lists of lists ready to be plotted on a subplot 


fig = plt.figure(figsize=(16,6))
ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 3)

ax3 = fig.add_subplot(4, 2, 2)
ax4 = fig.add_subplot(4, 2, 4)
ax5 = fig.add_subplot(4, 2, 6)
ax6 = fig.add_subplot(4, 2, 8)


ax1.plot(schedules[0][0],schedules[0][1])
ax1.set_ylabel('Interest Rate')

ax2.plot(schedules[1][0],schedules[1][1])
ax2.plot(schedules[2][0],schedules[2][1])
ax2.set_ylabel('Inflation')

ax3.plot(indices[0])
ax3.set_ylabel('Inflation Expectations')

ax4.plot(indices[1])
ax4.set_ylabel("Inflation")

ax5.plot(indices[2])
ax5.set_ylabel("Interest Rates")

ax6.plot(indices[3])
ax6.set_ylabel("GDP Index")


plt.savefig("Diagrams/IS_PC_MR_Curves_new.png")
plt.show()


